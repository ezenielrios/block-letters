# Sal's Shipping
# Rios

weight = 50

# Ground Shipping 🚚

if weight <= 2:
    cost_ground = weight * 2.5 + 20
elif weight <= 6:
    cost_ground = weight * 4.00 + 20
elif weight <= 10:
    cost_ground = weight * 5.00 + 20
else:
    cost_ground = weight * 5.75 + 20

print("Ground Shipping $", cost_ground)

# Ground Shipping Premium 🚚💨

cost_ground_premium = 150.00

print("Ground Shipping Premium $", cost_ground_premium)

# Drone Shipping 🛸

if weight <= 2:
    cost_drone = weight * 5.5
elif weight <= 6:
    cost_drone = weight * 10.00
elif weight <= 10:
    cost_drone = weight * 13.00
else:
    cost_drone = weight * 15.25

print("Drone Shipping: $", cost_drone)
